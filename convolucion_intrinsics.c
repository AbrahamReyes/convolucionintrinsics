#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <immintrin.h>

void convolucion_intrinsics(int *G,int *F,int *H,int n, int m, int sizeH){
	int i,j;
	int c = 0;

	/*Copiamos la imagen original a la imagen filtrada*/
	memcpy(G,F,n*m*sizeof(int));

	int aux , r, s , k , l;
	int N = (sizeH-1)/2; // recordando que sizeH = 2N+1 -> tamaño del filtro
	float *temp,*temp2; // almacenamiento auxiliar
	__m256 Vf,Vh,Vres,Vacc;

	// manejo de recursos
	// usaré registros vectoriales de 256 bits y me es mas fácil  
	// si temp y temp2 son multiplo de 8 (en longitud del vector)
	short temp_size;
	if (sizeH == 3)
		temp_size = 16; 
	else
		temp_size = 32;
	temp = (float *)calloc(temp_size,sizeof(float)); // como malloc pero inicializa temp a cero
	temp2 = (float *)calloc(temp_size,sizeof(float));

	/* Calcular suma de entradas de H */
	for(i = 0 ;i < sizeH; i++){
		for(j = 0;j < sizeH; j++){
			c += H[i*sizeH+j];
			temp2[i*sizeH+j] = (float)H[i*sizeH+j];
		}
	}
	if (c == 0)
		c = 1;
	/*** C o n v o l u c i ó n ***/
	for(i=N;i<n-N;i++){
		for(j=N;j<m-N;j++){
			aux=0;
			for (l = 0; l < 8; ++l)
				Vacc[l] = 0.0;	
/////////////////////////*** I n t r i n s i c s ***///////////////////////////

			// cargar submatrix en cuso hacia temp
			for(r=0;r<sizeH;r++)
				for(s=0;s<sizeH;s++)
					temp[r*sizeH+s] = (float)F[(i-N+r)*n+(j-N+s)];

			// procesamiento vectorial
			for (k = 0; k < temp_size; k += 8)
			{
				// no hay funciones para cargar enteros de 32 bits !!!!!! 
				// tampoco para multiplicarlos ni sumarlos usando avx
				// solo usando avx2 pero fallé al intentar usarlos aunque mi pc si soporta avx2 

				// usaré para flotantes , no me queda opción
				// sin embargo , el resultado es excelente 
				Vf = _mm256_load_ps(&temp[k]); // cargar 8 palabras de submatriz 
				Vh = _mm256_load_ps(&temp2[k]); // cargar 8 palabras de filtro

				Vres = _mm256_mul_ps(Vf,Vh); // multiplicar 8 palabras
				Vacc = _mm256_add_ps(Vacc,Vres); // acumulador de 8 palabras 	
			}
			
			for (l = 0; l < 8; ++l)
				aux += Vacc[l];	
//////////////////////////////////////////////////////////////////////////////
			G[i*n+j]=(int)((int)aux)/c;
		}
	}
	free(temp);
	free(temp2);
}


			
		