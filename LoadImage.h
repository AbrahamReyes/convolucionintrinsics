int  CargarImagen(int *F,int col, int row);
void print_vector(int *vector, int col, int row);
void print_archivo(int* matrix,int col, int row);

int  CargarImagen(int *F,int col, int row){
        FILE *entrada;
        entrada = fopen("buho.txt", "r");
        int i,j;
        if (entrada== NULL){
                perror("buho.txt");
                return EXIT_FAILURE;
        }
        for(i=0;i<col;i++){
                for(j=0;j<row;j++){
                        fscanf(entrada,"%d ",&F[i*row+j]);
                        //printf("%d ", F[i*row+j]);
                }
                //printf("\n");
        }
        fclose(entrada);
        return 0;
}

void print_vector(int *vector, int col, int row){
        int i,j;
        for(i=0;i<col;i++){
                for(j=0;j<row;j++){
                        printf("%d ",vector[i*row+j]);
                }
                printf("\n");
        }
}

void print_archivo(int* matrix,int col, int row){
        int i,j;
        FILE *archivo;/*El manejador de archivo*/
        archivo=fopen("resultado.txt", "w");
        for(i=0;i<col;i++){
                for(j=0;j<row;j++){
                        fprintf(archivo,"%d ",matrix[i*row+j]);
                }
		fprintf(archivo,"\n");
        }
        fclose(archivo);/*Cerramos el archivo*/
}

