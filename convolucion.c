//G:Imagen final
//F:Imagen inicial
//H:Filtro
void convolucion(int *G,int *F,int *H,int n, int m, int sizeH){
		int r,s,i,j;
		int c=0,aux=0;
		/* Calcular suma de entradas de H */
		for(i=0;i<sizeH;i++){
			for(j=0;j<sizeH;j++){
				c+=H[i*sizeH+j];
			}
		}
		if(c==0)
			c=1;
		printf("%d \n", c);
		/* Copiamos la imagen original a la imagen filtrada */ 
		for(i=0;i<m;i++){
			for(j=0;j<n;j++){
				G[i*m+j]=F[i*m+j];
			}								
		} 
		
		/*** C o n v o l u c i o n ***/
		int N = (sizeH-1)/2;
		for(i=N;i<n-N;i++){
			for(j=N;j<m-N;j++){
				aux=0;
				for(r=0;r<sizeH;r++){
					for(s=0;s<sizeH;s++){
						aux += F[(i-N+r)*n+(j-N+s)]*H[r*sizeH+s];
					}
				}
				G[i*n+j]=(int)aux/c;
			}
		}
}
