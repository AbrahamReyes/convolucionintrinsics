// Proyecto:  Covolución
// Created by: Pacheco, Abraham, Fernando
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int SeleccionarFiltro(int *H){
	// H debe ser una matriz de por lo menos 5x5
	// Conjunto de filtros disponibles 
	int k1 [3*3] = {0,-1,0,-1,5,-1, 0,-1,0};

	int k2 [3*3] = {1,1,1,1,1,1,1,1,1};

	int k3 [3*3] = {0,0,0,-1,1,0,0,0,0};

	int k4 [3*3] = {-2,-1,0,-1, 1,1,0, 1,2};

	int k5 [3*3] = {0, 1,0,1,-4,1,0, 1,0};

	int k6 [3*3] = {-1,0,1,-2,0,2,-1,0,1};

	int k7 [3*3] = { 1,-2, 1,-2, 5,-2,1,-2, 1};

	int k8 [3*3] = {1, 1, 1, 1,-2, 1,-1,-1,-1};

	int k9 [3*3] = {-1, 1,1,-1,-2,1,-1, 1,1};

	int k10 [5*5] = {1, 2, 3, 1,1  ,2, 7,11, 7,2,  3,11,17,11,3,  2, 7,11,7,1, 1, 2, 3, 2,1};

	// Pedir filtro al usuario 
	short Kernel_id = -1;
	printf("Seleccione el id del filtro:\n");
	printf("\n");
	printf("1.Enfoque\n");
	printf("2.Desenfoque\n");
	printf("3.Realce de bordes\n");
	printf("4.Repujado\n");
	printf("5.Detección de bordes\n");
	printf("6.Tipo Sobel\n");
	printf("7.Tipo Sharpen\n");
	printf("8.Norte\n");
	printf("9.Este\n");
	printf("10.Tipo Gauss\n");
	printf("\n");printf("Enter: ");
	while(Kernel_id > 10 || Kernel_id <= 0 ){
		scanf("%hd",&Kernel_id);
		if ( Kernel_id > 10 || Kernel_id <= 0 ){
			printf("Uppps!!! ingrese un id válido\n");
			printf("Enter: ");
		}
	}
	printf("El kernel que usted eligió fue : %d\n", Kernel_id);

	//Selección de kernel 
	int sizeH = 3;
    switch(Kernel_id)
    {
        case 1:
        	memcpy(H,k1,sizeH*sizeH*sizeof(int));
        	break;

        case 2:
        	memcpy(H,k2,sizeH*sizeH*sizeof(int));
        	break;

        case 3:
        	memcpy(H,k3,sizeH*sizeH*sizeof(int));
        	break;

        case 4:
        	memcpy(H,k4,sizeH*sizeH*sizeof(int));
        	break;

        case 5:
        	memcpy(H,k5,sizeH*sizeH*sizeof(int));
        	break;

        case 6:
        	memcpy(H,k6,sizeH*sizeH*sizeof(int));
        	break;

        case 7:
        	memcpy(H,k7,sizeH*sizeH*sizeof(int));
        	break;

        case 8:
        	memcpy(H,k8,sizeH*sizeH*sizeof(int));
        	break;

        case 9:
        	memcpy(H,k9,sizeH*sizeH*sizeof(int));
        	break;

        case 10:
        	sizeH = 5; 
        	memcpy(H,k10,sizeH*sizeH*sizeof(int));
        	break;

        // operator doesn't match any case constant (+, -, *, /)
        default:
            printf("Error! operator is not correct\n");
    }
	return sizeH;
}